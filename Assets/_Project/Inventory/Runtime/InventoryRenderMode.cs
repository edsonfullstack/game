namespace _Project.Inventory
{
    public enum InventoryRenderMode
    {
        Grid,
        Single,
    }
}