﻿using System.Collections.Generic;
using UnityEngine;

namespace _Project.Scriptable_Object
{
    [CreateAssetMenu(fileName ="New Item Database", menuName = "Scriptable Object/System/Database")]
    public class DataObject : ScriptableObject , ISerializationCallbackReceiver
    {
        public ItemObject[] Itens;
        public Dictionary<int, ItemObject> Get_Item= new Dictionary<int, ItemObject>();

        public void OnAfterDeserialize()
        {
            Get_Item= new Dictionary<int,ItemObject>();
            for (int i = 0; i < Itens.Length; i++)
            {
                if (Itens[i]!=null)
                { 
                    Get_Item.Add(i, Itens[i]);
                }
            }
        }

        public void OnBeforeSerialize()
        {
        }
    }
}
