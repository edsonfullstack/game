﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using Data;

using UnityEditor;
using UnityEngine;

namespace _Project.Scriptable_Object
{
	[CreateAssetMenu(fileName ="New Inventory System", menuName="Scriptable Object/System/Inventory")]
	public class InventoryObject : ScriptableObject,ISerializationCallbackReceiver
	{

		
		public DataObject database;


		public void AddItem(Slot _item, int _amount)
		{

	
		}
		

		public void OnBeforeSerialize()
		{
			
		}

		public void OnAfterDeserialize()
		{
			
			
		}
	}

	[System.Serializable]
	public class Slot
	{
		public int _ID;
		public ItemObject item;
		public int amount;


		public Slot(int _id,ItemObject _item,int _amount)
		{
			this._ID = _id;
			this.item = _item;
			this.amount=_amount;
		}
		public void AddAmout(int _amount){
			amount+=_amount;
		}
	}
}