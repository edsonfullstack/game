﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace _Project.Scriptable_Object
{
    public enum ItemType
    {
        Armor, //equipamentos
        Weapon, //equipamentos
        Consumable, //comida e potions
        Conteiner, //bags
        Default //
    }

    [CreateAssetMenu(fileName = "Abstract", menuName = "Scriptable Object/Items/Abstract")]
    public class ItemObject : ScriptableObject
    {
       public GameObject prefab;
        public ItemType type;
        public string Name;
        public Sprite uidisplay;
        [TextArea(15, 20)] public string description;
    }
}