﻿using UnityEngine;

namespace _Project.Scriptable_Object.Items
{
    [CreateAssetMenu(fileName ="New Armor Object",menuName="Scriptable Object/Items/Armor")]
    public class ArmorObject : ItemObject
    {
        public enum ArmorType
        {
            Clothes,
            LightArmor,
            MediumArmor,
            HeavyArmor
        }
        public int defense;
        public ArmorType armorType;
        public void Awake()
        {
            type = ItemType.Armor;
        }
    }
}
