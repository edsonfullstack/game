﻿using UnityEngine;

namespace _Project.Scriptable_Object.Items
{
    [CreateAssetMenu(fileName ="New Armor Object",menuName="Scriptable Object/Items/Armor")]
    public class ConsumableObject : ItemObject
    {
        public enum ConsumableType
        {
            Food,
            Potion
        }
        public int defense;
        public ConsumableType consumableType;
        public void Awake()
        {
            type = ItemType.Consumable;
        }
    }
}
