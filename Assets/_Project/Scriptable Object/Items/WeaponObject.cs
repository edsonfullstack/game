﻿using UnityEngine;

namespace _Project.Scriptable_Object.Items
{
    [CreateAssetMenu(fileName ="New Weapon Object",menuName="Scriptable Object/Items/Weapon")]
    public class WeaponObject : ItemObject
    {
        public enum DamageType
        {
            Piercing,
            Slashing,
            Bludgeoning
        }
        public Vector2 damage;
        public DamageType dmgtype;
        public void Awake()
        {
            type = ItemType.Weapon;
        }
    }
}
