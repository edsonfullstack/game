﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Project.Script.UI
{
    public class UILoader : MonoBehaviour
    {
        void Start()
        {
            if (SceneManager.GetSceneByName("UI2").isLoaded == false)
                LoadScene("UI2");
        }
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.I))
            {
                LoadScene("UI2");
            }

            if (Input.GetKeyDown(KeyCode.F2))
            {
                if (SceneManager.GetSceneByName("Level1").isLoaded == false)
                {
                    SceneManager.LoadSceneAsync("Level1", LoadSceneMode.Additive).completed += operation =>
                        SceneManager.SetActiveScene(SceneManager.GetSceneByName("Level1"));
                }
            }

            if (Input.GetKeyDown(KeyCode.F4))
            {
                if (SceneManager.GetSceneByName("UI").isLoaded == true)
                    SceneManager.UnloadSceneAsync("UI");
            }
        }

        private static void LoadScene(string name)
        {
            if (SceneManager.GetSceneByName(name).isLoaded == false)
                SceneManager.LoadSceneAsync(name, LoadSceneMode.Additive);
            else
                SceneManager.UnloadSceneAsync(name);
        }
    }
}
