﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace _Project.Script.Stats
{
    [System.Serializable]
    public struct Stat 
    {
        public int _str;
        public int _dex;
        public int _con;
        public int _int;
        public int _wil;
        public int _cha;

        public Stat(int _str = 10, int _dex = 10, int _con = 10, int _int = 10, int _wil = 10, int _cha = 10)
        {
            this._str = _str;
            this._dex = _dex;
            this._con = _con;
            this._int = _int;
            this._wil = _wil;
            this._cha = _cha;
        }

        public Stat(dynamic d)
        {
            this._str = d.forca;
            this._dex = d.dex;
            this._con = 0;
            this._int = 0;
            this._wil = 0;
            this._cha = 0;
        }

        public int Atacar()
        {
            return this._str * 10;
        }
    }
}