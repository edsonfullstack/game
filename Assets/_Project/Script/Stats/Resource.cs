﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace _Project.Script.Stats
{
    [System.Serializable]
    public struct Resource 
    {
        public long _hp;
        public long _mp;
        public long _sta;

        public Resource(int _hp = 100, int _mp = 0, int _sta = 6)
        {
            this._hp = _hp;
            this._mp = _mp;
            this._sta = _sta;

        }

        public void Execute(int index)
        {
            throw new System.NotImplementedException();
        }
    }
}
