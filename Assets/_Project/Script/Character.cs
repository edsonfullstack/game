﻿using Data;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using _Project.Script;
using _Project.Script.Stats;
using _Project.Scriptable_Object;
using UnityEngine;
using UnityEngine.UIElements;

using Newtonsoft.Json;


namespace _Project.Script
{
    public class Character : MonoBehaviour
    {
        [SerializeField] public string _user;
        [SerializeField] public string _password;
        [SerializeField] public Resource resource;
        [SerializeField] public Stat stat;
        [SerializeField] public MonoBehaviour go;
        [SerializeField] public InventoryObject equipment;
        [SerializeField] public InventoryObject backpack;


        public dynamic d;

        void Start()
        {
            d = JsonConvert.DeserializeObject("{\"Forca\":100,\"Dex\":100,\"Works\":false}");
            //_data.Load("");
            //resource = new Resource(_hp:this.stat._con*2);
        }

        void Update()
        {
            if (Input.GetKeyDown("a"))
            {
                print(d.Forca);
            }

            if (Input.GetKeyDown("l"))
            {

            }

            if (Input.GetKeyDown("k"))
            {
                print("FIRE:" + stat._str);
            }
        }

        void OnTriggerEnter(Collider other)
        {
            print(other.gameObject.name);
            var item = other.GetComponent<Item>();
            if (item)
            {
                backpack.AddItem(item.item, item.item.amount);
                Destroy(other.gameObject);
            }
        }

        void OnApplicationQuit()
        {
            //backpack.inventory.Clear();
        }
        [ContextMenu("Save")]
        public void Save()
        {
           print("Save"); 
        }
        [ContextMenu("Load")]
        public void Load()
        {
            print("Load"); 
        }
        [ContextMenu("Clear")]
        public void Clear()
        {
            print("Clear"); 
        }
    }
}